import { useState,useEffect, Fragment } from "react";
import { Container, Row, Col, Image, Button, Table } from "react-bootstrap";
import { Link } from "react-router-dom";

import CartImage from '../images/cart-img.png'

export default function OrderedProduct(){

    const [orders, setOrders] = useState([]);
    const [isOrderEmpty, setIsOrderEmpty] = useState(false)

    useEffect(() => {
        fetch(`${ process.env.REACT_APP_API_URL }/users/myOrder`, {
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(res => res.json())
        .then(data => {
            if (data.length > 0) {
                setIsOrderEmpty(false)
            } else {
                setIsOrderEmpty(true)
            }
            setOrders(data)
        })
    })

    return(
        <Container className="text-center mt-3">  
            <Row className="justify-content-center align-items-center">
                {
                    (isOrderEmpty)?
                    <Col lg={4}>
                        <Image src={CartImage}  className="cart-img" fluid/>
                        <h2>You have no orders yet!</h2>
                        <p>Looks like you haven't made your order yet. Go ahead & explore all products.</p>
                        <Button as={Link} to="/products/all" variant="primary" className="continue-shoppingBtn mt-3">Continue Shopping</Button>
                    </Col>
                    :
                    <Col>
                        <Container className='pt-3'>
                            <h1 className="pb-3">My Order</h1>
                            <Row className="justify-content-center align-items-center text-start">
                                <Col>
                                    <Table bordered responsive>
                                        <thead className='table-cart-header text-white text-center'>
                                            <tr>
                                                <th>Order no.</th>
                                                <th>Product</th>
                                                <th>Total</th>
                                                <th>Status</th>
                                            </tr>
                                        </thead>
                                        {
                                            orders.map(order => {
                                                const { _id, totalPrice, status } = order
                                                
                                                return(
                                                    <tbody key={_id}>
                                                        <tr>
                                                            <td>{_id}</td>
                                                            <td>
                                                            {
                                                                order.products.map(product => {

                                                                    const {productId, description, price, quantity} = product
                                                                    return(
                                                                        <Fragment key={productId}>
                                                                            <h6 className="m-0">{description}</h6>
                                                                            <p className="m-0">Unit Price : 
                                                                                <span className='order-unit-price'> ₱{price}</span>
                                                                            </p>
                                                                            <p>Quantity : 
                                                                                <span className='order-quantity'> {quantity}</span>
                                                                            </p>
                                                                            <br></br>
                                                                        </Fragment>
                                                                    )
                                                                })
                                                            }
                                                            </td>
                                                            <td className="order-total-price">₱{totalPrice}</td>
                                                            <td className="order-status">{status}</td>
                                                        </tr>
                                                    </tbody>
                                                )
                                            })
                                        }
                                    </Table>
                                </Col>
                            </Row>
                        </Container>
                    </Col>
                }
            </Row>
        </Container>
    )
}