import {Container, Row, Col} from 'react-bootstrap'
import { Link } from 'react-router-dom'

// Icons
import {SiFacebook, SiGitlab, SiGithub} from 'react-icons/si'

export default function Footer(){
    return(
        <Container className='footer-container mt-3 pt-5' fluid>
            <Row className='justify-content-center'>
                <Col lg={4}>
                    <h5 className='footer-subheader'>Categories</h5>
                    <Link to='/products/all' className='footer-category-button'>All product</Link>
                    <Link to='/products/Gaming Laptop' className='footer-category-button'>Gaming Laptops</Link>
                    <Link to='/products/Regular Laptop' className='footer-category-button'>Regular Laptops</Link>
                    <Link to='/products/Gaming Monitor' className='footer-category-button'>Gaming Monitors</Link>
                    <Link to='/products/Regular Monitor' className='footer-category-button'>Regular Monitors</Link>
                </Col>
                <Col lg={4}>
                    <h5 className='footer-subheader'>Contacts</h5>
                    <p className='footer-contact-info m-0'>Address : #64 Senatorial Road, Batasan Hills Quezon City</p>
                    <p className='footer-contact-info m-0'>Phone : +63 969 206 3705</p>
                    <p className='footer-contact-info m-0'>Email : lexushufano121097@gmail.com</p>
                    <SiFacebook className='footer-social-icons'/> 
                    <SiGitlab className='footer-social-icons'/> 
                    <SiGithub className='footer-social-icons'/>
                </Col>
            </Row>
            <Row className='footer-copyright justify-content-center align-items-center text-center mt-5 py-3'>
                <Col>
                    <p className='footer-copyright'>Copyright © 2022 All rights reserved | Lexus R. Hufano</p> 
                </Col>
            </Row>
        </Container>
    )
}