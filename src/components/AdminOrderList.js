import { Container, Row, Col, Table } from "react-bootstrap"
import { useState, Fragment } from "react"

export default function AdminOrderList(){

    const [orders, setOrders] = useState([]);
    const [isOrderListEmpty, setIsOrderListEmpty] = useState(false);

    useState(() => {
        fetch(`${process.env.REACT_APP_API_URL}/users/orders`, {
            headers: {
                Authorization: `Bearer ${ localStorage.getItem('token') }`
            }
        })
        .then(res => res.json())
        .then(data => {
            if (data.length > 0) {
                setIsOrderListEmpty(false)
            } else {
                setIsOrderListEmpty(true)
            }
            setOrders(data)
        })
    })

    return(
        <Container className="text-center bg-light">
            <Row className="justify-content-center align-items-center">
                {
                    (isOrderListEmpty)?
                    <>
                        <h1>No order found</h1>
                    </>
                    :
                    <Col>
                        <Container className='pt-3'>
                            <h1>Order List</h1>
                            <Row className="justify-content-center align-items-center text-start">
                                <Col>
                                    <Table bordered responsive>
                                        <thead className='table-cart-header text-white'>
                                            <tr>
                                                <th>Order no.</th>
                                                <th>Date Purchased</th>
                                                <th>Costumer</th>
                                                <th>Order</th>
                                                <th>Total Amount</th>
                                                <th>Status</th>
                                            </tr>
                                        </thead>
                                        {
                                            orders.map(order => {
                                                const { _id, purchasedOn, email, isDelivered, totalAmount,userId } = order
                                                return(
                                                    <tbody key={_id}>
                                                        <tr>
                                                            <td>{_id}</td>
                                                            <td>{purchasedOn}</td>
                                                            <td>
                                                                <p>User ID : {userId}</p>
                                                                <p>Email : {email}</p>
                                                            </td>
                                                            <td>
                                                            {
                                                                order.products.map(product => {
                                                                    const {productId, productName, price, quantity} = product
                                                                    return(

                                                                        <Fragment key={productId}>
                                                                            <h6 className="m-0">Product ID : {productId}</h6>
                                                                            <h6 className="m-0">Product Name : {productName}</h6> 
                                                                            <h6 className="m-0">Unit Price: ₱{price}</h6>
                                                                            <h6 className="m-0">Quantity: {quantity}</h6>
                                                                            <br></br>
                                                                        </Fragment>
                                                                    )
                                                                })
                                                            }
                                                            </td>
                                                            <td className="order-list-total-amount">₱{totalAmount}</td>
                                                            <td>{isDelivered}</td>
                                                        </tr>
                                                    </tbody>
                                                )
                                            })
                                        }
                                    </Table>
                                </Col>
                            </Row>
                        </Container>
                    </Col>
                }
            </Row>
        </Container>
    )
}