import { Container, Row, Col, Table } from "react-bootstrap";
import { useState, useEffect } from "react";

export default function AdminUsersList(){

    const [ users, setUsers] = useState([])
    const [ isUsersListEmpty, setIsUsersListEmpty] = useState(false)

    useEffect(()=> {
        fetch(`${process.env.REACT_APP_API_URL}/users`, {
            headers: {
                Authorization: `Bearer ${ localStorage.getItem('token') }`
            }
        })
        .then(res => res.json())
        .then(data => {
            if (data.length > 0) {
                setIsUsersListEmpty(false)
            } else {
                setIsUsersListEmpty(true)
            }
            setUsers(data)
        })
    })

    return(
        <Container className="text-center bg-light">
            <Row className='justify-content-center align-items-center'>
                
                    {
                        (isUsersListEmpty)?
                        <Col>
                            <h1>No user found</h1>
                        </Col>
                        :
                        <Col>
                            <Container className='pt-3'>
                                <h1>User List</h1>
                                <Row className="justify-content-center align-items-center text-start">
                                    <Col>
                                        <Table bordered responsive>
                                            <thead className='table-cart-header text-white'>
                                                <tr>
                                                    <th>User ID</th>
                                                    <th>Full name</th>
                                                    <th>Email</th>
                                                    <th>Mobile no.</th>
                                                    <th>Address</th>
                                                </tr>
                                            </thead>
                                            {
                                                users.map(user => {
                                                    const { _id, firstName, lastName, email, mobileNo, address} = user
                                                    return(
                                                        <tbody key={_id}>
                                                            <tr>
                                                                <td>{_id}</td>
                                                                <td>{firstName} {lastName}</td>
                                                                <td>{email}</td>
                                                                <td>{mobileNo}</td>
                                                                <td>{address}</td>
                                                            </tr>
                                                        </tbody>
                                                    )
                                                })
                                            }
                                        </Table>
                                    </Col>
                                </Row>
                            </Container>
                        </Col>
                    }
            </Row>
        </Container>
    )
}