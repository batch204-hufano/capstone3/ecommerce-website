import { NavLink } from 'react-router-dom'
import { Nav, Navbar, Container, NavDropdown, Dropdown, Image } from 'react-bootstrap'
import { useContext } from 'react';
import UserContext from '../UserContext';


// Images 
import WebsiteLogo from '../images/website-logo.png'

// icons
// import {BiSearch} from 'react-icons/bi';
import {BsCart} from 'react-icons/bs'
import {VscAccount} from 'react-icons/vsc'

export default function AppNavBar() {

    const { user } = useContext(UserContext);

    return(
        <Container className='app-navbar-container'>
            <Navbar>
                {/* Logo */}
                <Nav>
                    <Navbar.Brand as={NavLink} to='/'>
                        <Image src={WebsiteLogo} />
                    </Navbar.Brand>
                </Nav>
                <Nav>
                    <Nav.Link as={NavLink} to='/'>HOME</Nav.Link>
                    <Nav.Link as={NavLink} to='/products/all'>PRODUCTS</Nav.Link>

                    {
                        (user.isAdmin)?
                        <>
                        </>
                        :
                            <NavDropdown title="ALL CATEGORIES">
                                <NavDropdown.Item as={NavLink} to='/products/all'>ALL PRODUCTS</NavDropdown.Item>
                                <NavDropdown.Item as={NavLink} to='/products/Gaming Laptop'>GAMING LAPTOPS</NavDropdown.Item>
                                <NavDropdown.Item as={NavLink} to='/products/Regular Laptop'>REGULAR LAPTOPS</NavDropdown.Item>
                                <NavDropdown.Item as={NavLink} to='/products/Gaming Monitor'>GAMING MONITORS</NavDropdown.Item>
                                <NavDropdown.Item as={NavLink} to='/products/Regular Monitor'>REGULAR MONITORS</NavDropdown.Item>
                            </NavDropdown>
                    }
                </Nav>

                {/* All categories and seach bar */}
                {/* <Nav className='w-100'>
                    <Form className='w-100'>
                        <InputGroup className='navbar-search-bar '>
                            <Form.Control
                                type='search'
                                placeholder=''
                            />
                            <Button type='submit'><BiSearch /></Button>
                        </InputGroup>
                    </Form>
                </Nav> */}

                {/* Register, Login, Logout */}
                <Nav className='ms-auto'>
                    {
                        (user.id !== null)?
                            <>
                                {
                                    (user.isAdmin)?
                                    <>
                                        <Nav.Link as={NavLink} to='/dashboard' >DASHBOARD</Nav.Link>
                                        <Nav.Link as={NavLink} to='/logout' >LOGOUT</Nav.Link>
                                    </>
                                    :
                                    <>
                                        <Nav.Link as={NavLink} to='/cart'><BsCart className='cart-icon'/></Nav.Link>
                                        <Nav.Link><VscAccount className='account-icon'/></Nav.Link>
                                        <NavDropdown 
                                            align="end"
                                        >
                                            <NavDropdown.Item as={NavLink} to='/order'>My Order</NavDropdown.Item>
                                            <Dropdown.Divider />
                                            <Dropdown.Item as={NavLink} to='/logout'>Sign out</Dropdown.Item>
                                        </NavDropdown>
                                    </>
                                }
                            </>
                        :
                            <>
                                <Nav.Link as={NavLink} to='/register' >REGISTER</Nav.Link>
                                <Nav.Link as={NavLink} to='/login'>LOGIN</Nav.Link>
                            </>
                    }
                </Nav>
            </Navbar>
        </Container>
    )
}