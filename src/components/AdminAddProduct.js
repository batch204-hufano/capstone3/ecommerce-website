import {Container, Row, Col, Form, Button} from 'react-bootstrap'
import { useState } from 'react';

import Swal from 'sweetalert2';


export default function AdminAddProduct() {

    const [ productName, setProductName ] = useState('');
    const [ productDescription, setProductDescription ] = useState('');
    const [ productPrice, setProductPrice ] = useState(1);
    const [ productCategory, setProductCategory ] = useState('');
    const [ productStocks, setProductStocks ] = useState(1);
    const [ productImage, setProductImage] = useState('');

    const createProduct = (e) => {
        e.preventDefault();

        fetch(`${process.env.REACT_APP_API_URL}/products/add` , {
            method: 'POST',
            headers: {
                Authorization: `Bearer ${ localStorage.getItem('token') }`,
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                name: productName,
                description: productDescription,
                price: parseInt(productPrice),
                category: productCategory,
                stocks: parseInt(productStocks),
                image: productImage
            })
        })
        .then(res => res.json())
        .then(data => {
            if(data){
                Swal.fire({
                    title: "SUCCESS",
                    icon: "success",
                    text: "Product was successfully created.",
                    showConfirmButton: false,
                    timer: 2000
                })

                setProductName('')
                setProductDescription('')
                setProductPrice(1)
                setProductCategory('')
                setProductStocks(1)
                setProductImage('')
            } else {
                Swal.fire({
                    title: "Oops..",
                    icon: "error",
                    text: "Something went wrong. Please try again.",
                    showConfirmButton: false,
                    timer: 2000
                })
            }
        })
    }

    return(
        <Container className='pt-3 bg-light'>
            <h1 className='text-center'>Add product</h1>
            <Row className='justify-content-center align-items-center'>
                <Col lg={6}>
                    <Form onSubmit={(e) => createProduct(e)}>
                        <Form.Group className="mb-2" controlId="productName">
                            <Form.Label>Product name</Form.Label>
                            <Form.Control 
                                as="textarea"
                                minLength="10"
                                rows={2}
                                value={productName}
                                onChange={e => setProductName(e.target.value)}
                                required
                            />
                            <Form.Text className="text-muted">
                            Product name must be 20 characters or more.
                            </Form.Text>
                        </Form.Group>

                        <Form.Group className="mb-2" controlId="productDescription">
                            <Form.Label>Product description</Form.Label>
                            <Form.Control 
                                as="textarea" 
                                minLength="30" 
                                rows={4}
                                value={productDescription}
                                onChange={e => setProductDescription(e.target.value)}
                                required
                            />
                            <Form.Text className="text-muted">
                            Product description must be 30 characters or more.
                            </Form.Text>
                        </Form.Group>

                        <Form.Group className="mb-2" controlId="productPrice">
                            <Form.Label>Price</Form.Label>
                            <Form.Control 
                                type="number" 
                                min={1}
                                value={productPrice}
                                onChange={e => setProductPrice(e.target.value)}
                                required
                            />
                        </Form.Group>

                        <Form.Group className="mb-2" controlId="productStock">
                            <Form.Label>Stocks</Form.Label>
                            <Form.Control 
                                type="number" 
                                min={1} 
                                value={productStocks}
                                onChange={e => setProductStocks(e.target.value)}
                                required
                            />
                        </Form.Group>

                        <Form.Group className="mb-2" controlId="productCategory">
                            <Form.Label>Category</Form.Label>
                            <Form.Select 
                                aria-label="productCategory" 
                                value={productCategory}
                                onChange={e => setProductCategory(e.target.value)} 
                            >
                                <option>Please select...</option>
                                <option value="Gaming Laptop">Gaming Laptop</option>
                                <option value="Regular Laptop">Regular Laptop</option>
                                <option value="Gaming Monitor">Gaming Monitor</option>
                                <option value="Regular Monitor">Regular Monitor</option>
                            </Form.Select>
                        </Form.Group>

                        <Form.Group className="mb-3" controlId="productImage">
                            <Form.Label>Image(URL)</Form.Label>
                            <Form.Control 
                                type="text" 
                                value={productImage}
                                onChange={e => setProductImage(e.target.value)}
                                required
                            />
                        </Form.Group>

                        <Button className="w-100" type="submit">Create Product</Button>
                    </Form>
                </Col>
            </Row>
        </Container>
    )
}