import {Container, Row, Col, Image } from 'react-bootstrap'
import { Link } from 'react-router-dom'

// Images
import ShopByCategoriesImage from '../images/shop-by-categories-img.png'
import RegularLaptopCategoryImage from '../images/regular-laptop-category-img.png'
import GamingLaptopCategoryImage from '../images/gaming-laptop-category-img.png'
import GamingMonitorCategoryImage from '../images/gaming-monitor-category-img.png'
import RegularMonitorCategoryImage from '../images/regular-monitor-category-img.png'


export default function ShopByCategories() {
    return(
        <>
        <Container className='pt-3'>
            <Image src={ShopByCategoriesImage} className= 'w-100'/>
            <Row>
                <Col xs={6} lg={3} className= 'mb-3'>
                    <Link to='/products/Gaming Laptop'>
                        <Image src={GamingLaptopCategoryImage} fluid/>
                    </Link>
                </Col>
                <Col xs={6} lg={3} className= 'mb-3'>
                    <Link to='/products/Regular Laptop'>
                        <Image src={RegularLaptopCategoryImage} fluid/>
                    </Link>
                </Col>
                <Col xs={6} lg={3} className= 'mb-3'>
                    <Link to='/products/Gaming Monitor'>
                        <Image src={GamingMonitorCategoryImage} fluid/>
                    </Link>
                </Col>
                <Col xs={6} lg={3} className= 'mb-3'>
                    <Link to='/products/Monitor'>
                        <Image src={RegularMonitorCategoryImage} fluid/>
                    </Link>
                </Col>
            </Row>
        </Container>
        </>
    )
}