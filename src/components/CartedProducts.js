import { useEffect, useState } from "react"
import { Container, Row, Col, Image, Button, Table, Form } from "react-bootstrap";
import { Link } from "react-router-dom";
import Swal from "sweetalert2";
import {RiDeleteBin5Line} from 'react-icons/ri'

// Image
import CartImage from '../images/cart-img.png'

export default function CartedProducts() {

    const [products, setProducts] = useState([]);
    const [total, setTotal] = useState();
    const [isCartEmpty, setIsCartEmpty] = useState(false);

    useEffect(()=> {
        fetch(`${ process.env.REACT_APP_API_URL }/users/cart`, {
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(res => res.json())
        .then(data => {
            if (data.length > 0) {
                setIsCartEmpty(false)
            } else {
                setIsCartEmpty(true)
            }

            setProducts(data)
        })
    })

    useEffect(() => {
        fetch(`${ process.env.REACT_APP_API_URL }/users/cart/total`, {
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(res => res.json())
        .then(data => {
            if (data !== false) {
                setTotal(data.totalPrice)
            }
        })
    })

    const editItem = async (productId) => {
        const { value: number } = await Swal.fire({
            input: 'number',
            inputAttributes: {
                min: 1
            },
            inputLabel: 'Edit Quantity',
            confirmButtonColor: '#394867',
            confirmButtonText: 'Confirm',
            cancelButtonColor: '#9BA4B4',
            showCancelButton: true
        })

        if(number >= 1) {
            Swal.fire({
                title: 'Are you sure?',
                text: "Quantity will be modified after this!",
                icon: 'warning',
                showCancelButton: true,
                cancelButtonText : 'Cancel',
                confirmButtonColor: '#394867',
                cancelButtonColor: '#9BA4B4',
                confirmButtonText: 'Yes'
            })
            .then(result => {
                if(result.isConfirmed) {
                    fetch(`${process.env.REACT_APP_API_URL}/users/${productId}/cart`,{
                        method: 'PUT',
                        headers: {
                            'Content-Type': 'application/json',
                            Authorization: `Bearer ${localStorage.getItem('token')}`
                        },
                        body: JSON.stringify({
                            quantity: number
                        })
                    })
                    .then(res => res.json())
                    .then(data => {
                        if (data) {
                            Swal.fire({
                                title: "Success",
                                icon: "success",
                                text: "Quantity modified!",
                                showConfirmButton: false,
                                timer: 2000
                            })
                        } else {
                            Swal.fire({
                                title: "Something went wrong!",
                                icon: "error", 
                                text: "Please try again.",
                                showConfirmButton: false,
                                timer: 2000
                            })
                        }
                    })
                }
            })
        }
    }

    const removeProduct = (productId) => {
        Swal.fire({
            title: 'Deleting Item?',
            text: "Are you sure you would like to remove this item from your shopping cart?",
            icon: 'question',
            showCancelButton: true,
            confirmButtonColor: '#FF0000',
            cancelButtonColor: '#808080',
            confirmButtonText: 'Delete'
        })
        .then(result => {
            if(result.isConfirmed) {
                fetch(`${process.env.REACT_APP_API_URL}/users/${productId}/removeToCart` , {
                    method: 'DELETE',
                    headers: {
                        Authorization: `Bearer ${localStorage.getItem('token')}`
                    }
                })
                .then(res => res.json())
                .then(data => {
                    if(data) {
                        Swal.fire({
                            title: "Success",
                            icon: "success",
                            text: "Successfully remove the item to your cart.",
                            showConfirmButton: false,
                            timer: 2000
                        })
                    } else {
                        Swal.fire({
                            title: "Error",
                            icon: "error",
                            text: "Failed to remove item. Please try again.",
                            showConfirmButton: false,
                            timer: 2000
                        })
                    }
                })
            }
        })
    }

    const checkOutOrder = () => {
        Swal.fire({
            title: 'Are you sure?',
            text: "Order will be placed after this!",
            icon: 'question',
            showCancelButton: true,
            confirmButtonColor: '#394867',
            cancelButtonColor: '#9BA4B4',
            confirmButtonText: 'Yes'
        })
        .then(result => {
            if(result.isConfirmed) {
                fetch(`${ process.env.REACT_APP_API_URL }/users/checkout`, {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json',
                        Authorization: `Bearer ${localStorage.getItem('token')}`
                    }
                })
                .then(res => res.json())
                .then(data => {
                    if(data){
                        Swal.fire({
                            title: "Order Sucess!",
                            icon: "success",
                            text: "You have successfully ordered your items!",
                            showConfirmButton: false,
                            timer: 2000
                        })
                    } else {
                        Swal.fire({
                            title: "Something went wrong!",
                            icon: "error", 
                            text: "Please try again."
                        })
                    }
                })
            }
        })
    }

    return(
        <Container className="mt-3 text-center">
            <Row className="justify-content-center align-items-center">
                {
                    (isCartEmpty)?
                    <Col lg={4}>
                        <Image src={CartImage} fluid/>
                        <h2>Your cart is empty!</h2>
                        <p>Looks like you have not added anything to your cart. Go ahead & explore all products.</p>
                        <Button as={Link} to="/products/all" variant="primary" className="continue-shoppingBtn mt-3">Continue Shopping</Button>
                    </Col>
                    :
                    <Col>
                        <Container>
                            <h1 className="pb-3">My Cart</h1>
                            <Row className="justify-content-center align-items-center text-start">
                                <Col>
                                    <Table bordered responsive>
                                        <thead className='table-cart-header text-white'>
                                            <tr>
                                                <th>Product</th>
                                                <th className="text-center">Quantity</th>
                                                <th className="text-center">Subtotal</th>
                                            </tr>
                                        </thead>
                                        {
                                            products.map((product) => {
                                                const {productId, description, quantity, price} = product;
                                                return(
                                                    <tbody key={productId}>
                                                        <tr>
                                                            <td>
                                                                <div>
                                                                    <h6>{description}</h6>
                                                                    <p className="mb-2">Unit price : 
                                                                        <span className='cart-price'> ₱ {price}</span>
                                                                    </p>
                                                                    <RiDeleteBin5Line className='remove-product-button' onClick = {() =>removeProduct(productId)}>Remove</RiDeleteBin5Line>
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <Form.Control 
                                                                    className="cart-quantity text-center"
                                                                    type="number" 
                                                                    value={quantity}
                                                                    onChange= {() => editItem(productId)}
                                                                />
                                                            </td>
                                                            <td className="cart-price text-end">₱{quantity*price}</td>
                                                        </tr>
                                                    </tbody>
                                                )
                                            })
                                        }
                                        <tfoot>
                                            <tr className="text-end">
                                                <td className='cart-total-price' colSpan="2">TOTAL PRICE:</td>
                                                <td className='cart-total-price' >₱{total}</td>
                                            </tr>
                                        </tfoot>
                                    </Table>
                                </Col>
                            </Row>
                            <Row className='justify-content-center mt-3'>
                                <Col>
                                    <Button className='checkout-button' onClick={() => checkOutOrder()}>Proceed to Checkout</Button>
                                </Col>
                            </Row>
                        </Container>
                    </Col>
                }
            </Row>
        </Container>
    )
}