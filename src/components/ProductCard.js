import { Col, Card } from 'react-bootstrap'

export default function ProductCard({productProp}) {

    const {name, description, price} = productProp;


    return (
        <Col lg={3} md={4} className='p-1'>
            <Card>
                <Card.Body>
                    <Card.Title>{name}</Card.Title>
                    <Card.Text >{description.slice(0, 50)}...</Card.Text>
                    <Card.Text>PHP {price}</Card.Text>
                </Card.Body>
            </Card>
        </Col>
    )
}