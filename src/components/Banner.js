import {Container, Carousel} from 'react-bootstrap'
import Banner1 from '../images/banner1.png'
import Banner2 from '../images/banner2.png'
import Banner3 from '../images/banner3.png'
import Banner4 from '../images/banner4.png'

export default function Banner() {
    return(
        <Container className='p-0' fluid>
            <Carousel>
                <Carousel.Item>
                    <img
                    className="d-block w-100"
                    src={Banner3}
                    alt="Banner3"
                    />
                </Carousel.Item>
                <Carousel.Item>
                    <img
                    className="d-block w-100"
                    src={Banner1}
                    alt="Banner1"
                    />
                </Carousel.Item>
                <Carousel.Item>
                    <img
                    className="d-block w-100"
                    src={Banner4}
                    alt="Banner4"
                    />
                </Carousel.Item>
                <Carousel.Item>
                    <img
                    className="d-block w-100"
                    src={Banner2}
                    alt="Banner2"
                    />
                </Carousel.Item>
            </Carousel>
        </Container>
    )
}