import { useState, useEffect } from 'react';
import { Container, Row, Col, Table, Button, Modal, Form } from 'react-bootstrap';

export default function AdminView(props){

	const { productsProp, fetchData } = props;

	const [productsArr, setProductsArr] = useState([])
	const [productId, setProductId] = useState("")
	const [name, setName] = useState("")
	const [description, setDescription] = useState("")
	const [price, setPrice] = useState(0)
    const [stocks, setStocks] = useState(0)
    const [category, setCategory] = useState("")
	const [image, setImage] = useState("")

	const [showEdit, setShowEdit] = useState(false)

	const token = localStorage.getItem("token")

	//Functions to handle opening and closing modals
	const openEdit = (productId) => {
		fetch(`${process.env.REACT_APP_API_URL}/products/id/${productId}`)
		.then(res => res.json())
		.then(data => {
			setProductId(data._id)
			setName(data.name)
			setDescription(data.description)
			setPrice(data.price)
            setStocks(data.stocks)
            setCategory(data.category)
			setImage(data.image)
		})
		setShowEdit(true)
	}

	const closeEdit = () => {
		setProductId('')
		setName('')
		setDescription('')
		setPrice(0)
        setStocks(0)
        setCategory('')
		setImage('')
		setShowEdit(false)
	}

	const editProduct = (e) => {
		e.preventDefault()

		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}` , { 
			method: 'PUT',
			headers: { 
				"Content-type": "application/json",
				Authorization: `Bearer ${token}`
			},
			body: JSON.stringify({
				name: name,
				description: description,
				price: price,
                stocks: stocks,
                category: category,
				image: image
			})
		})
		.then(res => res.json())
		.then(data => {
			if (data){
				alert('Product successfully updated')

				closeEdit()

				fetchData()
			}else {
				alert('Something went wrong')
			}
		})
	}

	const archiveToggle = (productId, isActive) => {
		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/archive` , { 
			method: 'PUT',
			headers: { 
				"Content-type": "application/json",
				Authorization: `Bearer ${token}`
			},
			body: JSON.stringify({
				isActive: !isActive
			})
		})
		.then(res => res.json())
		.then(data => {
			if (data){
				let bool

				isActive ? bool = 'disabled' : bool = 'enabled'
				
				alert(`Product successfully ${bool}`)

				fetchData()
			} else {
				alert('Something went wrong')
			}
		})
	}

	useEffect(() => {
		//map through the coursesProp to generate table contents
		const products = productsProp.map(product => {
			return(
				<tr key={product._id}>
					<td>{product.name}</td>
					<td>{product.description}</td>
					<td>{product.price}</td>
                    <td>{product.stocks}</td>
                    <td>{product.category}</td>
					<td>
							{/*Dynamically render course availability*/}
							{product.isActive
								? <span>Available</span>
								: <span>Unavailable</span>
							}
					</td>
					<td>{product.image}</td>
					<td>
						<Button className='mx-1' variant="primary" size="sm" onClick={() => openEdit(product._id)}>Update</Button>
						{product.isActive
							//dynamically render which button show depending on course availability
							? <Button className='mx-1' variant="danger" size="sm" onClick={() => archiveToggle(product._id, product.isActive)}>Disable</Button>
							: <Button className='mx-1' variant="success" size="sm" onClick={() => archiveToggle(product._id, product.isActive)}>Enable</Button>
						}
					</td>
				</tr>
			)
		})

		//set the CoursesArr state with the results of our mapping so that it can be used in the return statement
		setProductsArr(products)

	}, [productsProp])

	return(
		<Container className='mt-3'>
			<Row>
				<Col>
					<h1 className='text-center mb-3'>Admin Dashboard</h1>
					{/*Course info table*/}
					<Table className='bg-light' bordered hover responsive>
						<thead className="table-dashbord-header text-white">
							<tr>
								<th>Name</th>
								<th>Description</th>
								<th>Price</th>
								<th>Stocks</th>
								<th>Category</th>
								<th>Availability</th>
								<th>Image(URL)</th>
								<th>Actions</th>
							</tr>
						</thead>
						<tbody>
							{/*Mapped table contents dynamically generated from the coursesProp*/}
							{productsArr}
						</tbody>
					</Table>

					{/*Edit Course Modal*/}
					<Modal show={showEdit} onHide={closeEdit}>
						<Form onSubmit={e => editProduct(e)}>
							<Modal.Header closeButton>
								<Modal.Title>Update Product</Modal.Title>
							</Modal.Header>

							<Modal.Body>
								<Form.Group controlId="productName">
									<Form.Label>Name</Form.Label>
									<Form.Control
										value={name}
										onChange={e => setName(e.target.value)}
										type="text"
										required
									/>
								</Form.Group>

								<Form.Group controlId="productDescription">
									<Form.Label>Description</Form.Label>
									<Form.Control
										value={description}
										onChange={e => setDescription(e.target.value)}
										type="text"
										required
									/>
								</Form.Group>

								<Form.Group controlId="productPrice">
									<Form.Label>Price</Form.Label>
									<Form.Control
										value={price}
										onChange={e => setPrice(e.target.value)}
										type="number"
										required
									/>
								</Form.Group>

								<Form.Group controlId="productPrice">
									<Form.Label>Stocks</Form.Label>
									<Form.Control
										value={stocks}
										onChange={e => setStocks(e.target.value)}
										type="number"
										required
									/>
								</Form.Group>

								<Form.Group controlId="productCategory">
									<Form.Label>Category</Form.Label>
									<Form.Control
										value={category}
										onChange={e => setCategory(e.target.value)}
										type="text"
										required
									/>
								</Form.Group>

								<Form.Group controlId="productImage">
									<Form.Label>Image(URL)</Form.Label>
									<Form.Control
										value={image}
										onChange={e => setImage(e.target.value)}
										type="text"
										required
									/>
								</Form.Group>
							</Modal.Body>

							<Modal.Footer>
								<Button variant="secondary" onClick={closeEdit}>Close</Button>
								<Button variant="success" type="submit">Submit</Button>
							</Modal.Footer>
						</Form>
					</Modal>
				</Col>
			</Row>
		</Container>
	)
}
