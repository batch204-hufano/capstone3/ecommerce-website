import { BrowserRouter as Router, Route, Routes} from 'react-router-dom'
import { useState, useEffect } from 'react';

// external CSS
import './App.css';

// components
import AppNavBar from './components/AppNavBar';

// pages
import Cart from './pages/Cart';
import Dashboard from './pages/Dashboard';
import Error from './pages/Error';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Register from './pages/Register';
import Home from './pages/Home'
import Order from './pages/Order';
import Product from './pages/Product';
import SpecificProduct from './pages/SpecificProduct';

import { UserProvider } from './UserContext';

function App() {

  const [ user, setUser ] = useState({
    id : null,
    isAdmin : null
  })

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/users/profile`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`
      }
    })
    .then(res => res.json())
    .then(data => {
      if(typeof data._id !== 'undefined'){
        setUser({
          id: data._id,
          isAdmin: data.isAdmin
        })
      }else{
        setUser({
          id: null,
          isAdmin: null
        })
      }
    })
  }, [])


  return (
    <UserProvider value={{user, setUser}}>
      <Router>
        <AppNavBar />
        <Routes>
          <Route exact path="/" element= {<Home/>} />
          <Route exact path='/products/:category' element={<Product />} />
          <Route exact path="/products/id/:productId" element= {<SpecificProduct />} />
          <Route exact path='/login' element={<Login />} />
          <Route exact path='/register' element={<Register />} />
          <Route exact path='/logout' element={<Logout />} />
          <Route exact path="/cart" element= {<Cart />} />
          <Route exact path="/order" element= {<Order />} />
          <Route exact path="/dashboard" element= {<Dashboard />} />
          <Route exact path='*' element={<Error />} />
        </Routes>
      </Router>
    </UserProvider>
  )
}

export default App;
