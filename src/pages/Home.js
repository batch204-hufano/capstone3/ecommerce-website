import Banner from "../components/Banner"
import Footer from "../components/Footer"
import ShopByCategories from "../components/ShopByCategories"

export default function Home() {
    return(
        <>
            <Banner />
            <ShopByCategories />
            <Footer />
        </>
    )
}