import { Container } from "react-bootstrap";
import OrderedProduct from "../components/OrderedProducts";
import UserContext from "../UserContext";
import { useContext } from "react";
import { Navigate } from "react-router-dom";

export default function Order(){

    const {user} = useContext(UserContext)

    return(
        (user.id !== null) ?
            <Container>
                <OrderedProduct />
            </Container>
        :
            <Navigate to ="/login" />
    )
}