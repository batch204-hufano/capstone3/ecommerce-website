import {Tabs, Tab, Container, Row, Col} from "react-bootstrap"
import AdminAddProduct from "../components/AdminAddProduct"
import AdminOrderList from "../components/AdminOrderList"
import AdminUsersList from "../components/AdminUsersList"
import { useContext } from "react"
import UserContext from "../UserContext"
import { Navigate } from "react-router-dom"

export default function Dashboard(){

    const {user} = useContext(UserContext)

    return(
        (user.isAdmin)?
        <Container>
            <Row className="justify-content-center mt-3">
                <Col>
                    <h1 className="text-center">Admin Dashboard</h1>
                    <Tabs
                        defaultActiveKey="add-products"
                        id="uncontrolled-tab-example"
                        justify
                    >
                        <Tab eventKey="orders" title="Orders" className='p-3 border border-top-0 bg-light'>
                            <AdminOrderList />
                        </Tab>
                        <Tab eventKey="add-products" title="Add Products" className='p-3 border border-top-0 bg-light'>
                            <AdminAddProduct />
                        </Tab>
                        <Tab eventKey="users" title="Users" className='p-3 border border-top-0 bg-light'>
                            <AdminUsersList />
                        </Tab>
                    </Tabs>
                </Col>
            </Row>
        </Container>
        :
        <>
            <Navigate to='/' />
        </>
    )
}