import { useContext } from "react"
import UserContext from "../UserContext"
import { Container } from "react-bootstrap"
import { Navigate } from "react-router-dom"
import CartedProducts from "../components/CartedProducts"

export default function Cart() {

    const {user} = useContext(UserContext)

    return(
        (user.id !== null) ?
            <Container>
                <CartedProducts />
            </Container>
        :
        <Navigate to ="/login" />
    )
}