import { Container, Row, Col, Image } from "react-bootstrap"

// Images
import ErrorPageImage from '../images/error-page-img.png' 

export default function Error(){
    return(
        <Container>
            <Row className='justify-content-center'>
                <Col xs={12} md={6}>
                    <Image src={ErrorPageImage} fluid/>
                </Col>
            </Row>
        </Container>
    )
}