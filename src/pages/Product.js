import { Link, useParams } from "react-router-dom"
import { Container, Row, Col, Card } from "react-bootstrap";
import UserContext from '../UserContext.js'
import { useContext, useEffect, useState } from "react"
import AdminView from "../components/AdminView.js";

export default function Product() {
    const[ productsData, setProductsData ] = useState([])

    const { user } = useContext(UserContext)

    let { category } = useParams();

    const fetchData = () => {
        fetch(`${process.env.REACT_APP_API_URL}/products/all`)
		.then(res => res.json())
		.then(data => {
			setProductsData(data)
		})
    }

    useEffect(() => {
        fetchData()
    },[])

    const data = productsData.filter(product => {
        if(category === 'all') {
            return product;
        } else {
            return product.category === category
        }
        
    })

    return (
        (user.isAdmin) ?
		<AdminView productsProp={productsData} fetchData={fetchData}/>
		:
        <Container className='mt-3'>
            <Row>
                <Col xs={4} lg={2}>
                    <h5>Categories</h5>
                    <Link to='/products/all' className='category-button'>All product</Link>
                    <Link to='/products/Gaming Laptop' className='category-button'>Gaming Laptops</Link>
                    <Link to='/products/Regular Laptop' className='category-button'>Regular Laptops</Link>
                    <Link to='/products/Gaming Monitor' className='category-button'>Gaming Monitors</Link>
                    <Link to='/products/Regular Monitor' className='category-button'>Regular Monitors</Link>
                </Col>
                <Col md={10}>
                    <Container>
                        <Row>
                            {data.map((product)=> {
                                const { _id, name, description, price, image, isActive} = product;
                                if(isActive) {
                                    return (
                                        <Col sm={6} md={4} lg={3} className='product-card p-1' key={_id}>
                                            <Link className='button-product-details' to={`/products/id/${_id}`}>
                                                <Card className='product-card h-100'>
                                                    <Card.Img variant="top" src={image} />
                                                    <Card.Body className='d-flex flex-column'>
                                                        <Card.Title className='mt-auto'>{name.slice(0,20)}...</Card.Title>
                                                        <Card.Text className="text-justify mb-2">{description.slice(0,70)}...</Card.Text>
                                                        <Card.Text className='product-card-price'>₱ {price}</Card.Text>
                                                    </Card.Body>
                                                    
                                                </Card>
                                            </Link>
                                        </Col>
                                    )
                                } else {
                                    return null
                                }
                            })}
                        </Row>
                    </Container>
                </Col>
            </Row>
        </Container>
    )
}