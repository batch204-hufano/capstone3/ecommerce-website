import { useState, useEffect, useContext } from 'react';
import { Container, Row, Col, Image, InputGroup, Button, Form } from 'react-bootstrap';
import { useParams, useNavigate, Link } from 'react-router-dom'
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function SpecificCourse() {

    const {user} = useContext(UserContext);
    const navigate = useNavigate();

    const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);
    const [stocks, setStocks] = useState(0);
    const [category, setCategory] = useState("");
    const [image, setImage] = useState("");

    const [products, setProducts] = useState([]);

    const [quantity, setQuantity] = useState(1);
	
	const {productId} = useParams();



    // Add qty
    const decrementQty = () => {
        if(quantity > 1) {
            setQuantity(prevCount => prevCount - 1)
        }
    }

    // Minus qty
    const incrementQty = () => {
        if(quantity < stocks) {
            setQuantity(prevCount => prevCount + 1)
        }
    }

    // Quantity
    const inputQty = (value) => {
		setQuantity(value);
	}

    useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/products/id/${productId}`)
		.then(res => res.json())
		.then(data => {
            setProducts(data)
			setName(data.name)
			setDescription(data.description)
			setPrice(data.price)
            setStocks(data.stocks)
            setCategory(data.category)
            setImage(data.image)
		})
	}, [])

    const addToCart = () => {
        if(products.stocks > 0) {
            fetch(`${process.env.REACT_APP_API_URL}/users/${productId}/addToCart`, {
                method: 'POST',
                headers: {
                    Authorization: `Bearer ${localStorage.getItem('token')}`,
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    quantity: quantity
                })
            })
            .then(res => res.json())
            .then(data => {
                if(data) {

                    Swal.fire({
                        title: 'Item added to cart!',
                        text: "Do you want to see your added carts?",
                        icon: 'success',
                        showCancelButton: true,
                        confirmButtonColor: '#394867',
                        cancelButtonColor: '#9BA4B4',
                        confirmButtonText: 'Yes'
                    })
                    .then(result => {
                        if(result.isConfirmed){
                            
                            navigate("/cart")
                        }
                    })
                } else {
                    Swal.fire({
                        title: "Server Error",
                        icon: "error",
                        text: "Please try again",
                        showConfirmButton: false,
                        timer: 2000
                    })
                }
            })
        }
    }

    const loginFirst = () => {
        Swal.fire({
            title: "Error",
            icon: "error",
            text: "You must login first.",
            showConfirmButton: false,
            timer: 2000
        })
    }

    return(
        <Container className='mt-3'>
            <Row className='single-product-page-row justify-content-center align-items-center text-justify'>
                <Col lg={6} >
                    <Image src={image} fluid/>
                </Col>
                <Col lg={6}>
                    <p className='p-0 m-0'>Product Description:</p>
                    <h4 className='mb-3'>{description}</h4>
                    <p className='single-product-page-price'>₱ {price}.00</p>
                    <p className='mb-1'>Quantity: {stocks} piece available</p>
                    <InputGroup className='w-50 mb-3'>
                        <Button className='single-product-page-quantityBtn' onClick={decrementQty}>-</Button>
                        <Form.Control 
                            className='text-center'
                            value={quantity}
                            onChange={e => inputQty(e.target.value)}
                        />
                        <Button className='single-product-page-quantityBtn' onClick={incrementQty}>+</Button>
                    </InputGroup>
                    {
                    (user.id !== null)?   
                        <Button className='add-to-cartBtn' onClick = {addToCart}>Add to Cart</Button>
                    :   
                        <Button as={Link} to="/login" className='add-to-cartBtn' onClick={loginFirst}>Add to Cart</Button>
                    }
                </Col>
            </Row>
        </Container>
    )
}
