import { useState, useEffect, useContext } from 'react'
import { Container, Row, Col, Image, Form, FloatingLabel, Button } from 'react-bootstrap'
import { Navigate, useNavigate, Link } from 'react-router-dom'
import UserContext from '../UserContext';

import Swal from 'sweetalert2';

// Images
import LoginPhoto from '../images/login-img.png' 

export default function Login() {

    const [ email, setEmail ] = useState ('');
    const [ password, setPassword ] = useState ('');
    const [isActive, setIsActive] = useState(false);

    const { user, setUser } = useContext(UserContext);

    const navigate = useNavigate();

    useEffect(() => {

        if (email !== '' && password !== '') {
            setIsActive(true)
        } else {
            setIsActive(false)
        }
    }, [ email, password ])

	const retrieveUserDetails = (token) => {
		fetch(`${process.env.REACT_APP_API_URL}/users/profile` , {
			headers: {
				Authorization: `Bearer ${token}`
			}
		})
		.then(res => res.json())
		.then(data => {
			
			setUser({
				id: data._id,
				isAdmin: data.isAdmin
			})
		})
	}

	function authenticateUser(e){
		e.preventDefault()

		fetch(`${process.env.REACT_APP_API_URL}/users/login` , {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			}, 
			body : JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(res => res.json())
		.then(data => {
			if(typeof data.access !== 'undefined'){
				localStorage.setItem('token', data.access)

				retrieveUserDetails(data.access)

                Swal.fire({
                    title: "Login Successful",
                    icon: "success",
                    text: "Hey there! Welcome to our store.",
                    showConfirmButton: false,
                    timer: 2000
                })

				navigate('/')
			} else {

                Swal.fire({
                    title: "Auth failed",
                    icon: "error",
                    text: "Login failed. Please try again.",
                    showConfirmButton: false,
                    timer: 2000
                })
			}
		})

		setEmail('')
		setPassword('')
	}

    return(
        (user.id !== null) ?
            <Navigate to='/' />
        :
        <Container className='login-container'>
            <Row className='justify-content-center align-items-center text-center'>
                <Col xs={5} >
                    <Image src={LoginPhoto} fluid/>
                </Col>
                <Col xs={5}>
                    <h2>Welcome Back!</h2>
                    <p>Sign in to continue</p>
                    <Form onSubmit={e => authenticateUser(e)} className='login-form'>
                        <Container fluid>
                            <Row>
                                <Col xs={12} className='my-2'>
                                    <FloatingLabel controlId="email" label="Email">
                                        <Form.Control 
                                            type='email' 
                                            placeholder='Email' 
                                            value = {email}
                                            onChange = {e => setEmail(e.target.value)}
                                            required/>
                                    </FloatingLabel>
                                </Col>
                                <Col xs={12} className='my-2'>
                                    <FloatingLabel controlId="password" label="Password">
                                        <Form.Control 
                                        type='password' 
                                        placeholder='Password' 
                                        value = {password}
                                        onChange = {e => setPassword(e.target.value)}
                                        required/>
                                    </FloatingLabel>
                                </Col>
                            </Row>
                        </Container>
                        {
                            isActive?
                                <Button className='login-button' type='submit'>LOG IN</Button>
                            :
                                <Button className='login-button' disabled>LOG IN</Button>
                        }
                    </Form>
                    <p>Don't have account? <Link to='/register'>Create a new account</Link></p>
                </Col>
            </Row>
        </Container>
    )
}