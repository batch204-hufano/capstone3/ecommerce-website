import { useState, useEffect, useContext } from 'react';
import { Container, Row, Col, Image, Form, FloatingLabel, Button } from 'react-bootstrap';
import { Navigate, useNavigate, Link } from 'react-router-dom';
import UserContext from '../UserContext';

import Swal from 'sweetalert2';

// Images
import RegisterPhoto from '../images/register-img.png' 

export default function Register(){

    const [ firstName, setFirstName ] = useState ('');
    const [ lastName, setLastName ] = useState ('');
    const [ email, setEmail ] = useState ('');
    const [ mobileNo, setMobileNo ] = useState ('');
    const [ password1, setPassword1 ] = useState ('');
    const [ password2, setPassword2 ] = useState ('');
    const [ address, setAddress ] = useState ('');
    const [isActive, setIsActive] = useState(false);

    const navigate = useNavigate();

    const { user } = useContext(UserContext);

    useEffect(() => {

        if ((firstName !== '' && lastName !== '' && email !== '' && mobileNo.length === 11 && password1 !== '' &&  password2 !== '' &&  address !== '') && (password1 === password2)) {
            
            setIsActive(true)
        } else {

            setIsActive(false)
        }
    },[firstName, lastName, email, mobileNo, password1, password2, address])

    function registUser(e){
        e.preventDefault() 

        fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail` , {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: email
            })
        })
        .then(res => res.json())
        .then(data => {
            if(data){

                Swal.fire({
                    title: "ERROR",
                    icon: "error",
                    text: "Duplicate email exists. Please try again.",
                    showConfirmButton: false,
                    timer: 2000
                })
            } else {
                fetch(`${process.env.REACT_APP_API_URL}/users/register` , {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({
                        firstName: firstName,
                        lastName: lastName,
                        email: email,
                        mobileNo: mobileNo,
                        password: password1,
                        address : address
                    })
                })
                .then(res => res.json())
                .then(data => {
                    if(data){

                        Swal.fire({
                            title: "SUCCESS",
                            icon: "success",
                            text: "Congratulations, your account has been successfully created.",
                            showConfirmButton: false,
                            timer: 2000
                        })

                        navigate('/login')
                    } else {

                        Swal.fire({
                            title: "Oops..",
                            icon: "error",
                            text: "Something went wrong. Please reload the page.",
                            showConfirmButton: false,
                            timer: 2000
                        })
                    }
                })
            }
        })
    }


    return(
        (user.id !== null) ?
            <Navigate to="/" />
        :
        <Container className='register-container'>
            <Row className='justify-content-center align-items-center text-center'>
                <Col lg={5} >
                    <Image src={RegisterPhoto} fluid/>
                </Col>
                <Col lg={5}>
                    <h2>Create an account</h2>
                    <Form onSubmit = {e=> registUser(e)} className='register-form'>
                        <Container fluid>
                            <Row>
                                <Col xs={6} className='my-2'>
                                    <FloatingLabel controlId="firstName" label="First Name">
                                        <Form.Control 
                                            type='text' 
                                            placeholder='First Name' 
                                            value={firstName}
                                            onChange={e => setFirstName(e.target.value)}
                                            required/>
                                    </FloatingLabel>
                                </Col>
                                <Col xs={6} className='my-2'>
                                    <FloatingLabel controlId="lastName" label="Last Name">
                                        <Form.Control 
                                            type='text' 
                                            placeholder='Last Name' 
                                            value={lastName}
                                            onChange={e => setLastName(e.target.value)}
                                            required/>
                                    </FloatingLabel>
                                </Col>
                                <Col xs={12} className='my-2'>
                                    <FloatingLabel controlId="email" label="Email">
                                        <Form.Control 
                                            type='email' 
                                            placeholder='Email' 
                                            value={email}
                                            onChange={e => setEmail(e.target.value)}
                                            required/>
                                    </FloatingLabel>
                                </Col>
                                <Col xs={12} className='my-2'>
                                    <FloatingLabel controlId="mobileNo" label="Mobile Number">
                                        <Form.Control 
                                        type='text' 
                                        placeholder='Mobile Number' 
                                        value={mobileNo}
                                        onChange={e => setMobileNo(e.target.value)}
                                        required/>
                                    </FloatingLabel>
                                </Col>
                                <Col xs={12} className='my-2'>
                                    <FloatingLabel controlId="password1" label="Password">
                                        <Form.Control 
                                            type='password' 
                                            placeholder='Password' 
                                            value={password1}
                                            onChange={e => setPassword1(e.target.value)}
                                            required/>
                                    </FloatingLabel>
                                </Col>
                                <Col xs={12} className='my-2'>
                                    <FloatingLabel controlId="password2" label="Re-enter password">
                                        <Form.Control 
                                            type='password' 
                                            placeholder='Re-enter password'
                                            value={password2}
                                            onChange={e => setPassword2(e.target.value)} 
                                            required/>
                                    </FloatingLabel>
                                </Col>
                                <Col xs={12} className='my-2'>
                                    <FloatingLabel controlId="address" label="Address">
                                        <Form.Control 
                                        type='text' 
                                        placeholder='Address' 
                                        value={address}
                                        onChange={e => setAddress(e.target.value)}
                                        required/>
                                    </FloatingLabel>
                                </Col>
                            </Row>
                        </Container>

                        {
                            isActive? 
                                <Button className='login-button'type='submit'>CONTINUE</Button>
                            :
                                <Button className='login-button'disabled>CONTINUE</Button>
                        }
                        
                    </Form>
                    <p>Already have an account? <Link to='/login'>Log In</Link></p>
                </Col>
            </Row>
        </Container>
    )
}